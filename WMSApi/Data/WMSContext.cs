﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMSApi.Models;

namespace WMSApi.Data
{
    public class WMSContext : DbContext
    {
        public WMSContext(DbContextOptions<WMSContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserProfile>()
                .HasOne<Warehouse>(s => s.Warehouse)
                .WithMany(g => g.UserProfile)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<UserLocationJoin>().HasKey(sc => new { sc.UserProfileId, sc.WarehouseLocationId });

            modelBuilder.Entity<UserLocationJoin>()
                .HasOne<WarehouseLocation>(sc => sc.WarehouseLocation)
                .WithMany(s => s.UserLocationJoin)
                .HasForeignKey(sc => sc.WarehouseLocationId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<UserLocationJoin>()
                .HasOne<UserProfile>(sc => sc.UserProfile)
                .WithMany(s => s.UserLocationJoin)
                .HasForeignKey(sc => sc.UserProfileId)
                .OnDelete(DeleteBehavior.ClientSetNull);

        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<WarehouseLocation> WarehouseLocations { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Inventory> Inventorys { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<UserLocationJoin> UserLocationJoin { get; set; }

    }
}
