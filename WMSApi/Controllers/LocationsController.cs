﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WMSApi.Data;
using WMSApi.Models;

namespace WMSApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Locations")]
    [Authorize]
    public class LocationsController : Controller
    {
        private readonly WMSContext _context;

        public LocationsController(WMSContext context)
        {
            _context = context;
        }

        // GET: api/Locations
        [HttpGet]
        public IEnumerable<WarehouseLocation> GetWarehouseLocations()
        {
            return _context.WarehouseLocations;
        }

        // GET: api/Locations/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetWarehouseLocation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var warehouseLocation = await _context.WarehouseLocations.SingleOrDefaultAsync(m => m.WarehouseLocationId == id);

            if (warehouseLocation == null)
            {
                return NotFound();
            }

            return Ok(warehouseLocation);
        }

        // PUT: api/Locations/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWarehouseLocation([FromRoute] int id, [FromBody] WarehouseLocation warehouseLocation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != warehouseLocation.WarehouseLocationId)
            {
                return BadRequest();
            }

            _context.Entry(warehouseLocation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WarehouseLocationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Locations
        [HttpPost]
        public async Task<IActionResult> PostWarehouseLocation([FromBody] WarehouseLocation warehouseLocation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.WarehouseLocations.Add(warehouseLocation);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetWarehouseLocation", new { id = warehouseLocation.WarehouseLocationId }, warehouseLocation);
        }

        // DELETE: api/Locations/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWarehouseLocation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var warehouseLocation = await _context.WarehouseLocations.SingleOrDefaultAsync(m => m.WarehouseLocationId == id);
            if (warehouseLocation == null)
            {
                return NotFound();
            }

            _context.WarehouseLocations.Remove(warehouseLocation);
            await _context.SaveChangesAsync();

            return Ok(warehouseLocation);
        }

        private bool WarehouseLocationExists(int id)
        {
            return _context.WarehouseLocations.Any(e => e.WarehouseLocationId == id);
        }
    }
}