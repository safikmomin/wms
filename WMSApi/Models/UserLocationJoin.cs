﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WMSApi.Models
{
    public class UserLocationJoin
    {
        public int WarehouseLocationId { get; set; }
        public WarehouseLocation WarehouseLocation { get; set; }

        public int UserProfileId { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}
