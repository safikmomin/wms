﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WMSApi.Models
{
    public class Order
    {
        [Key]
        public int OrderId { get; set; }

        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public string IsDeleted { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModificationClientTime { get; set; }
        public DateTime LastModificationUTCTime { get; set; }

        [ForeignKey("Customer")]
        public int CustomerId { get; set; }

        public Customer Customer { get; set; }
        public ICollection<Inventory> Inventory { get; set; }
        public ICollection<OrderItem> OrderItem { get; set; }
    }
}
