﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WMSApi.Models
{
    public class Inventory
    {
        [Key]
        public int InventoryId { get; set; }

        public DateTime Date { get; set; }

        public string BarCode { get; set; }
        public string IsDeleted { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModificationClientTime { get; set; }
        public DateTime LastModificationUTCTime { get; set; }

        [ForeignKey("Item")]
        public int ItemId { get; set; }

        [ForeignKey("Order")]
        public int? OrderId { get; set; }

        [ForeignKey("WarehouseLocation")]
        public int WarehouseLocationId { get; set; }

        public WarehouseLocation WarehouseLocation { get; set; }

        public Order Order { get; set; }

        public Item Item { get; set; }
    }
}
