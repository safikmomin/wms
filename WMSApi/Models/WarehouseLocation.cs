﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WMSApi.Models
{
    public class WarehouseLocation
    {
        public int WarehouseLocationId { get; set; }
        public string PhoneNumber { get; set; }
        public string LocationName { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string IsDeleted { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModificationClientTime { get; set; }
        public DateTime LastModificationUTCTime { get; set; }

        [ForeignKey("Warehouse")]
        public int WarehouseId { get; set; }

        public Warehouse Warehouse { get; set; }
        public ICollection<UserLocationJoin> UserLocationJoin { get; set; }
        public ICollection<Inventory> Inventory { get; set; }
    }
}
