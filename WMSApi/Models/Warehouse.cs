﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WMSApi.Models
{
    public class Warehouse
    {
        [Key]
        public int WarehouseId { get; set; }

        public string Name { get; set; }
        public string IsDeleted { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModificationClientTime { get; set; }
        public DateTime LastModificationUTCTime { get; set; }
        
        public ICollection< UserProfile> UserProfile { get; set; }
        public ICollection<WarehouseLocation> WarehouseLocation { get; set; }
        public ICollection<Customer> Customer { get; set; }
    }
}
