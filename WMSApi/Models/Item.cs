﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WMSApi.Models
{
    public class Item
    {
        [Key]
        public int ItemId { get; set; }

        public string Name { get; set; }
        public string Code { get; set; }
        public string PricingModel { get; set; }
        public int InStock { get; set; }
        public string Price { get; set; }
        public decimal Weight { get; set; }
        public string IsDeleted { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModificationClientTime { get; set; }
        public DateTime LastModificationUTCTime { get; set; }

        public ICollection<Inventory> Inventory { get; set; }

        public ICollection<OrderItem> OrderItem { get; set; }
    }
}
