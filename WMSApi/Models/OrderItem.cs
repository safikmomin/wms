﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WMSApi.Models
{
    public class OrderItem
    {
        [Key]
        public int OrderItemId { get; set; }

        public decimal Quantity { get; set; }
        public string QuantityType { get; set; }
        public string IsDeleted { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModificationClientTime { get; set; }
        public DateTime LastModificationUTCTime { get; set; }

        [ForeignKey("Item")]
        public int ItemId { get; set; }
        [ForeignKey("Order")]
        public int OrderId { get; set; }

        public Item Item { get; set; }
        public Order Order { get; set; }
    }
}
